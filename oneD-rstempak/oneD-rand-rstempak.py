#oneD-rand-rstempak.py
import sys
import random

def main():

    #gather arguments and check for proper usage
    args = sys.argv[1:]
    if len(sys.argv) != 2:
        print ("Usage: python oneD-rand-rstempak.py <seed>")
        return

    #get data from config file
    config = open("config.txt", "r+")
    rawdata = config.readline().strip()
    wolfram_num, CA_length = rawdata.split(",")

    #intialize CA
    seed = args[0]
    random.seed(seed)
    CA = [0] * int(CA_length)

    for index in range(int(CA_length)):
        if random.random() > 0.5:
            CA[index] = 1

    #print to screen and write to config file
    print(",".join(str(p) for p in CA))
    config.write(",".join(str(p) for p in CA))
    config.close()

if __name__ == '__main__':
    main()
