#oneD-run-rstempak.py
import sys

def display(p):
    if p == "1":
        return "X"
    else:
        return " "

def next_step(CA, wolfram_num):
    new_CA = CA[:] #create output CA

    for i, _ in enumerate(CA):
        #gather neighbor data
        current_cell = CA[i]
        right_cell = 0
        left_cell = 0
        if i-1 < 0:
            left_cell = CA[len(CA) - 1]
        else:
            left_cell = CA[i-1]

        if i+1 == len(CA):
            right_cell = CA[0]
        else:
            right_cell = CA[i+1]

        #update cell
        rule_position = 7 - int((left_cell + current_cell + right_cell), 2)
        new_CA[i] = wolfram_num[rule_position]

    return new_CA

def main():

    #gather arguments and check for proper usage
    args = sys.argv[1:]
    if len(sys.argv) != 2:
        print ("Usage: python oneD-run-rstempak.py <steps to be simulated>")
        return

    #get data from config file
    config = open("config.txt", "r+")
    rawdata = config.readline()
    wolfram_num, CA_length = rawdata.strip().split(",")
    raw_CA = config.readline().strip()
    CA = raw_CA.split(",")
    config.close()

    #update CA and print first line
    steps = args[0]
    print(" ".join(display(p) for p in CA))

    for i in range(int(steps)):
        CA = next_step(CA, wolfram_num)
        print(" ".join(display(p) for p in CA))

        #rewrite confiq file with final state
        if i == (int(steps) - 1):
            with open("config.txt", "w") as config:
                data = []
                data.append(rawdata)
                data.append(",".join(str(p) for p in CA))
                config.writelines(data)


if __name__ == '__main__':
    main()
