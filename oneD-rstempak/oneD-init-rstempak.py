#oneD-init-rstempak.py
import sys

def main():

    #gather arguments and check for proper usage
    args = sys.argv[1:]
    if len(sys.argv) != 2:
        print ("Usage: python oneD-init-rstempak.py <list of indices to initialize as 1 seperated by commas>")
        return

    #get data from config file
    config = open("config.txt", "r+")
    rawdata = config.readline().strip()
    wolfram_num, CA_length = rawdata.split(",")

    #intialize CA
    indeces = [index for index in args[0].split(",")]
    CA = [0] * int(CA_length)
    for index in indeces:
        CA[int(index)] = 1

    #print to screen and write to config file
    print(",".join(str(p) for p in CA))
    config.write(",".join(str(p) for p in CA))
    config.close()

if __name__ == '__main__':
    main()
