#oneD-define-rstempak.py
import sys

def main():
    args = sys.argv[1:]
    if len(sys.argv) != 3:
        print ("Usage: python oneD-define-rstempak.py <Wolfram Rule Number> <length of CA>")
        return

    Wolfram_Num = args[0]
    length = args[1]
    Wolfram_Bin = format(int(Wolfram_Num), '08b')
    print("{},{}").format(Wolfram_Bin, length)

    with open("config.txt", "w+") as config:
        config.write("{},{}\n".format(Wolfram_Bin, length))

if __name__ == '__main__':
    main()
